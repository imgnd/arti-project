<?php

use Imgnd\Arti\Route;

/**
 * For each accessible uri create a routing link to set which method is needed to call.
 * Put the full link without domain name and including leading slash as first parameter.
 * Trailing slashes will automatically be removed by default, this can be changed in ".htaccess".
 * In the url will parts between curly brackets be converted to parameters.
 * Example: on visiting "/users/42/profile" will use "/users/{id}/profile" as link and set "42" as value for the variable $id.
 * The second parameter must be a callable action, array with class and method or a string containing the Class@method.
 * When using a callable method as second parameter, an optional third parameter can be added. This must be an array.
 * The parameters from curly brackets will be merged with these and passed to the method.
 * Adding default parameters to urls can be helpful on setting or checking rights or roles for users.
 */

Route::link('/', [BaseController::class, 'home']);
