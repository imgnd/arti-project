# Imagined Arti Project

[![Latest Stable Version](https://img.shields.io/packagist/v/imgnd/arti-project)](https://packagist.org/packages/imgnd/arti-project)
[![Total Downloads](https://img.shields.io/packagist/dt/imgnd/arti-project)](https://packagist.org/packages/imgnd/arti-project)
[![License](https://img.shields.io/packagist/l/imgnd/arti-project)](https://packagist.org/packages/imgnd/arti-project)
[![php](https://img.shields.io/badge/php->7.4-green.svg)](https://packagist.org/packages/imgnd/arti-project)

A skeleton project using [imgnd/arti](https://gitlab.com/imgnd/arti) : A lightweight micro php framework


## Installation

Install using `composer create-project imgnd/arti-project [project-name]`. Fill in the environment settings in the ".env" file.\
The skeleton project uses Tailwind as CSS framework by default. Run `npm install` to install all dependencies and `npx tailwindcss init` to use Tailwind.\
Run `npx tailwindcss -i ./assets/css/style.css -o ./assets/css/style.min.css --minify` to build the final minified css.

## Usage

Put your classes in the "controllers" folder. In "routes/web.php" you can set which method must be called on an url.\
All files for getting data from and set into the database can be placed in the "models" folder. Use the shortcut `DB::class` for rapid development.\
The views can be made follow the [Blade Template Engine](https://laravel.com/docs/8.x/blade) standards and are rendered with the standalone version [BladeOne](https://github.com/EFTEC/BladeOne). \
It is recommended to put public accessible files in the "assets" folder. In all directories are sample files with minimal content present.

## Routing

For each accessible uri create a routing link to set which method is needed to call.\
Put the full link without domain name and including leading slash  as first parameter.\
Trailing slashes will automatic be removed by default, this can be changed in ".htaccess".\
In the url will parts between curly brackets be convert to parameters.\
Example: on visiting "/users/42/profile" will use "/users/{id}/profile" as link and set "42" as value for the variable $id.\
The second parameter must be a callable action or a string containing the Class@method.\
When using a string as second parameter can an optional third parameter be added. This must be an array.\
The parameters from curly brackets will be merged with these and passed to the method.\
Adding default parameters to urls can be helpful on setting or checking rights or roles for users.

## Helpful functions

The "imgnd/arti" framework contains some helpful functions for rapid development.\
In the "vendor/imgnd/arti" folder can the code of this project be found.
> See the `Imgnd\Arti\Helper` class for some supportive methods. Some are directly callable as php functions, these are defined in functions.php
- The `dump()` function is easy for development. This function prints a `var_dump()` within `<pre>` tags, so the data is clearly readable.
- All get, post and session data is readable as object in the super global variables `$get`, `$post` and `$session`.
- Get an environment setting with the `env()` function.
- A full storage path can be retrieved with the `file_path()` function.
- The full url to a file can be generated by the `public_url()` function.
- With `View::get()` you can get the html of a template part. With `View::render()` you display a full template.
- Use the `DB::class` functions to generate and execute SQL queries.

### License

The Arti skeleton project and Arti framework are open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
