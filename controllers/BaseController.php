<?php

use Imgnd\Arti\View;

class BaseController
{
    /**
     * Homepage
     * @throws Exception
     */
    public function home()
    {
        View::render('page.welcome');
    }
}