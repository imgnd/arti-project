<!DOCTYPE html>
<html lang="{{ env('APP_LANG') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ env('APP_NAME') . (isset($title) ? ' | ' . $title : '') }}</title>
    <link rel="stylesheet" href="{{ public_url('assets', 'css', 'style.min.css') }}">
</head>
<body class="bg-gradient-to-br from-cyan-500 via-cyan-700 to-teal-700">
<div class="sm:container sm:mx-auto">