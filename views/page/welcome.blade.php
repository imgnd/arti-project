@extends('main')
@section('content')
    <div class="min-h-screen py-32">
        <div class="text-center text-xl font-thin tracking-tight sm:max-w-xl sm:mx-auto">
            <h1 class="decoration-clone bg-clip-text bg-gradient-to-b from-slate-50 to-slate-200 text-transparent text-7xl small-caps leading-normal">Arti</h1>
            <p class="text-lg">A lightweight micro php framework</p>
        </div>
    </div>
@stop