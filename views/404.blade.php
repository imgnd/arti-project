<!DOCTYPE html>
<html lang="{{ env('APP_LANG') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>404 | {{ env('APP_NAME') }}</title>
    <link rel="stylesheet" href="{{ public_url('assets', 'css', 'style.min.css') }}">
</head>
<body class="font-sans bg-gradient-to-b from-slate-500 via-slate-700 to-slate-900">
<div class="sm:container sm:mx-auto">
    <div class="min-h-screen py-32">
        <div class="text-center text-xl text-white font-thin tracking-tight sm:max-w-xl sm:mx-auto">
            <h1 class="decoration-clone bg-clip-text bg-gradient-to-br from-cyan-300 via-cyan-500 text-transparent text-7xl small-caps leading-normal">404</h1>
            <p class="text-lg text-slate-50">Page not found</p>
            <a href="/" title="Return to home" class="text-base text-slate-300 underline">Return to home</a>
        </div>
    </div>
</div>
</body>
</html>